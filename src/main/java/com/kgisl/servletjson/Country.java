package com.kgisl.servletjson;

/**
 * country
 */
public class Country {
private int id;
private String name;
/**
 * @return the id
 */
public int getId() {
    return id;
}
/**
 * @return the name
 */
public String getName() {
    return name;
}
/**
 * @param id the id to set
 */
public void setId(int id) {
    this.id = id;
}
/**
 * @param name the name to set
 */
public void setName(String name) {
    this.name = name;
}
@Override
public String toString() {
    return id+""+name+"";
}
    
}